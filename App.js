
import React from 'react';
import Navigation from './Navigation/Navigation'
import Store from './Store/configureStore'
import {Provider} from 'react-redux'
export default function App() {
  return (
    <Provider store={Store}>
      <Navigation />
    </Provider>
    )
  
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
