// Components/FilmDetail.js

import React from 'react'
import { StyleSheet, View, Text, ActivityIndicator, ScrollView, Image, Button } from 'react-native'
import { getDetailsFilmFromApi, getImageFromApi } from '../API/TMDApi'
import moment from 'moment'
import numeral from 'numeral'
import { connect} from 'react-redux'
import { TouchableOpacity } from 'react-native-gesture-handler'
class FilmDetails extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      film: undefined,
      isLoading: true
    }
  }

  //   componentDidMount()
  //   {
  //       const idFilm = this.props.route.params.idFilm
  //       getDetailsFilmFromApi(idFilm).then(data => {
  //     this.setState({
  //       film: data,
  //       isLoading: false
  //     })
  //   })
  // }
  componentDidMount()
  {
    const { idFilm } = this.props.route.params;
    const favoriteFilmIndex = this.props.favoriteFilm.findIndex(item => item.id === idFilm)
    if (favoriteFilmIndex !== -1) {  this.setState({
        film: this.props.favoriteFilm[favoriteFilmIndex]
      })
      return
    }
    this.setState({ isLoading: true })

    getDetailsFilmFromApi(idFilm
      ).then(data =>
      {
      this.setState({
        film: data,
        isLoading: false
      })
    })
  }
  _displayLoading() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
  }
  _addToFavoris()
  {
    const action = { type: 'TOGGLE_FAVORITE', value: this.state.film }
    this.props.dispatch(action)
    
  }
  _showFavorite()
  {
    var displayIcon = require('../Images/ic_favorite_border.png')
    if (this.props.favoriteFilm.findIndex(item => item.id === this.state.film.id) !== -1)
    {
      displayIcon = require('../Images/ic_favorite.png')
    }
    return (
      <Image style={{height : 40, width : 40}} source={displayIcon} ></Image>
    )
  }
  componentDidUpdate()
  {
    console.log(this.props.favoriteFilm);
  }
  _displayFilm() {
    const { film } = this.state
    if (film != undefined) {
      return (
        <ScrollView style={{
            flex: 1
          }}>
          <Image
            style={{
                height: 180,
                margin: 5
              }}
            source={{uri: getImageFromApi(film.backdrop_path)}}
          />
          <Text style={ {
             fontWeight: 'bold',
            fontSize: 35,
            flex: 1,
            flexWrap: 'wrap',
            marginLeft: 5,
            marginRight: 5,
            marginTop: 10,
            marginBottom: 10,
            color: '#000000',
            textAlign: 'center'
          }}>{film.title}</Text>
          <TouchableOpacity style={{ alignItems: 'center' }} onPress={()=> this._addToFavoris()}>
            {this._showFavorite()}
          </TouchableOpacity>
          <Text style={{
            fontStyle: 'italic',
            color: '#666666',
            margin: 5,
             marginBottom: 15
            }}>{film.overview}</Text>
          <Text style={ {
             marginLeft: 5,
            marginRight: 5,
            marginTop: 5,
            }}>Sorti le {moment(new Date(film.release_date)).format('DD/MM/YYYY')}</Text>
          <Text style={ {
            marginLeft: 5,
            marginRight: 5,
            marginTop: 5,
            }}>Note : {film.vote_average} / 10</Text>
          <Text style={ {
            marginLeft: 5,
            marginRight: 5,
             marginTop: 5,
            }}>Nombre de votes : {film.vote_count}</Text>
          <Text style={ {
                marginLeft: 5,
            marginRight: 5,
            marginTop: 5,
             }}>Budget : {numeral(film.budget).format('0,0[.]00 $')}</Text>
          <Text style={ {
            marginLeft: 5,
            marginRight: 5,
            marginTop: 5,
            }}>Genre(s) : {film.genres.map(function(genre){
              return genre.name;
            }).join(" / ")}
          </Text>
          <Text style={ {
            marginLeft: 5,
            marginRight: 5,
            marginTop: 5,
            }}>Companie(s) : {film.production_companies.map(function(company){
              return company.name;
            }).join(" / ")}
          </Text>
        </ScrollView>
      )
    }
  }

  render() {
    return (
      <View style={styles.main_container}>
        {this._displayLoading()}
        {this._displayFilm()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  scrollview_container: {
    flex: 1
  },
  image: {
    height: 169,
    margin: 5
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 35,
    flex: 1,
    flexWrap: 'wrap',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    marginBottom: 10,
    color: '#000000',
    textAlign: 'center'
  },
  description_text: {
    fontStyle: 'italic',
    color: '#666666',
    margin: 5,
    marginBottom: 15
  },
  default_text: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
  }
})
const MapStateToProps = (state) =>
{
  return {
    favoriteFilm : state.favoriteFilm
  }
}
//export default FilmDetails
export default connect(MapStateToProps)(FilmDetails)