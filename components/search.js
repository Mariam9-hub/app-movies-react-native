import React from 'react'
import { StyleSheet, View, Button, TextInput,Text, FlatList,SafeAreaView, StatusBar,ActivityIndicator} from 'react-native'
import data from '../helpers/filmsData'
import FilmItem from './filmItem'
import FilmList from './FilmList'
import {getFilmsFromApiWithSearchedText} from'../API/TMDApi'
import { connect } from 'react-redux'

// const Item = ({ title }) => (
//     <View style={styles.item}>
//       <Text style={styles.title}>{title}</Text>
//     </View>
// );
// const renderItem = ({ item }) => (
//     <Item title={item.title} />
//   );
class Search extends React.Component
{
    constructor(props)
    {
        super(props)
        this.page = 0;
        this.totalPage = 0;
        this.state = {
            films: [],
            isLoding: false
        }
        this.searchedText = ""
    }
    
    _loadFilms=()=>{
        this.setState({isLoding:true})
        this.searchedText.length > 0 ? getFilmsFromApiWithSearchedText(this.searchedText, this.page+1).then((data) =>
        {
            this.page = data.page;
            this.totalPage = data.total_pages;
            this.setState({
                films: [...this.state.films,...data.results],
                isLoding: false
            })
        }) : null
    }
    _searchedFilm(text)
    {
        this.searchedText = text;
    }
    _loadingData()
    { if(this.state.isLoding)
       { return (
            <View style={{ position: 'absolute', top: 100, left:0,right:0,bottom:0, alignItems: 'center', justifyContent:'center'}}>
                <ActivityIndicator color="#4087e5"  size="large" />
            </View>
        )}
    }
    _searchFilms()
    {
        this.totalPage = 0;
        this.page = 0;
        this.setState({
            films:[]
        },()=>this._loadFilms())
        
    }
    
    render()
    {
        console.log(this.props);
       
        console.log("data :", this.state.films)
        return (
            <View style={styles.mainContainer}>
                <TextInput onSubmitEditing={()=>this._searchFilms()} onChangeText={(text)=>this._searchedFilm(text)} style={styles.TextInput} placeholder="Titre du film"/>
                <Button title="Rechercher" onPress={() => this._searchFilms()} />
                <FilmList
                    films={this.state.films}
                    page={this.page}
                    totalPage={this.totalPage}
                    loadFilms={this._loadFilms}
                    favoriteList={false} 
                    navigation={this.props.navigation}
                />
                {/* <FlatList
                    data={this.state.films}
                    extraData={this.props.favoriteFilm}
                    renderItem={({ item }) => <FilmItem film={item} displayDetailsForFilm={_displayDetailForFIlm}/>}
                        keyExtractor={item => item.id.toString()}
                        onEndReachedThreshold={0.5 }
                        onEndReached={() =>{ 
                         console.log('onEndReached');  
                        //    (this.page<this.totalPage) && this._loadFilms()
                        if (this.page < this.totalPage){
                            this._loadFilms()
                        }
                        }}
                /> */}
                {this._loadingData()}
            </View>
            
            //  <View style={{ flex : 1,  backgroundColor:'yellow', justifyContent: 'center' , alignItems:'center',flexDirection:'row'}}>
            //     <View style={{ height : 50, width : 50, backgroundColor: 'green' }} ></View>
            //     <View style={{ height: 50, width: 50,backgroundColor: 'red' }} ></View>
            //     <View style={{ height : 50, width : 50, backgroundColor: 'blue'  }} ></View>
            //  </View>
            //
            // <View style={{ flex : 1,backgroundColor: 'yellow', flexDirection : 'row'}}>
            //     <View style={{ flex : 1, backgroundColor: 'green' }} ></View>
            //     <View style={{ flex : 2, }} ></View>
            //  </View>
      )
  }  
}

    const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
      },
      item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
      },
    mainContainer: {
        flex: 1,
        //backgroundColor :'yellow'
    },
    TextInput: {
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5,
        height: 50,
        borderColor: '#000000',
        borderWidth: 1,
        paddingLeft: 5
    }
})
const MapStateToProps = (state) =>
{
  return {
    favoriteFilm : state.favoriteFilm
  }
}
export default connect(MapStateToProps)(Search);