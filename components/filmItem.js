import React from 'react'
import { Text, View , StyleSheet,Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import { getImageFromApi } from '../API/TMDApi'
import {connect} from 'react-redux'
class FilmItem extends React.Component
{
    _loadFilmImage(name)
    {
        getImageFromApi(name)
    }
    _showIfFavoriteFilm(idFilm)
    {
        console.log(this.props.favoriteFilm);
        if (this.props.favoriteFilm.findIndex(item => item.id === idFilm) > -1){
            return (
                <Image style={{height : 40, width : 40}} source={require('../Images/ic_favorite.png')} ></Image>
           )
       }
   }
    render()
    {
        const {film , displayDetailsForFilm,}=this.props;
        return (
            
        <TouchableOpacity onPress={()=>displayDetailsForFilm(film.id)} style={{flex : 1, flexDirection: 'row'}}>
            <View style={{ flexDirection: 'row', height: 180, flex: 1, backgroundColor: 'grey',  marginTop: 5, marginLeft: 5, alignItems: 'center', justifyContent: 'center'}}>
            <Image style={{ height: 180, width: "100%" }} source={{ uri: getImageFromApi(film.poster_path) }}></Image>
            </View>
            <View style={{ flex: 2, }}>
            <View style={{flex : 1,  marginTop:5, marginLeft:5, marginRight:5}}>
                <View style={{ flex: 1 , flexDirection:'row'}}>
                        <View style={{ flex: 2, marginLeft: 5, flexDirection: 'row'}}>
                        <View>{ this._showIfFavoriteFilm(film.id) }</View>
                            <Text style={{ fontWeight: 'bold', color: '#000000', fontSize: 20, flexWrap:'wrap' }}>{ film.original_title }</Text>
                        </View>
                        <View style={{flex: 1,alignItems:'flex-end',marginRight:5 }}>
                                <Text style={[styles.title_text, { fontWeight: 'bold', fontSize: 26, height: 30, color: '#666666' }]}>{ film.vote_average }</Text>
                        </View>
                </View> 
                <View style={{ flex: 7}}>
                            <Text style={{ marginLeft: 5, color: '#000000', fontStyle: 'italic' , flexWrap:'wrap'}} numberOfLines={6}>{ film.overview }</Text>
                 </View>
                    <View style={{ flex: 1, justifyContent:'center', alignItems:'flex-end' }}>
                            <Text style={{ marginRight: 5, color: '#000000' }}>Sortie le { film.release_date }</Text>
                    </View>  
                
            </View>
                </View>
                
        </TouchableOpacity>
            // <View style={{flex : 1, flexDirection: 'row'}}>
            //     <View style={{ flexDirection: 'row', height: 180, flex: 1, backgroundColor: 'grey', borderWidth: 1, marginTop: 5, marginLeft: 5, alignItems: 'center', justifyContent: 'center'}}>
            //         <Text>Image du film</Text>
            //     </View>
            //     <View style={{ flex: 2, }}>
            //     <View style={{flex : 1, backgroundColor:'green',  marginTop:5, marginLeft:5, marginRight:5}}>
            //         <View style={[styles.main_container, { flex: 2, backgroundColor: '#edaa9a' , flexDirection:'row'}]}>
            //                 <View style={{flex : 2, marginLeft:5, justifyContent:'center' }}>
            //                 <Text style={styles.title_text}>Title</Text>
            //                 </View>
            //                 <View style={{flex: 1,alignItems:'flex-end',marginRight:5,justifyContent:'center'}}>
            //                 <Text style={[styles.title_text]}>Vote</Text>
            //                 </View>
            //         </View> 
            //         <View style={{ flex: 8, backgroundColor: 'pink',justifyContent:'center'}}>
            //             <Text style={{marginLeft:5}} numberOfLines={6}>Description</Text>
            //          </View>
            //             <View style={{ flex: 2, backgroundColor: '#cd7cdd', justifyContent:'center', alignItems:'flex-end' }}>
            //             <Text style={{marginRight:5}}>Sortie le 00/00/000000</Text>
            //             </View>  
                    
            //     </View>
            //  </View>
            // </View>
            
           
        )
    }
    
}
const styles = StyleSheet.create({
    main_container: {
     height : 30   
    },
    title_text: {
        
    }
})
const MapStateToProps = (state) =>
{
  return {
    favoriteFilm : state.favoriteFilm
  }
}
export default connect(MapStateToProps)(FilmItem);