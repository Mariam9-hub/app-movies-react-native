import React from 'react'
import FilmList from './FilmList'
import { FlatList, StyleSheet, Text, View } from 'react-native'
import FilmItem from './filmItem'
import{connect } from 'react-redux'
function Favorites(props)
{
    // const _displayDetailForFIlm = (idFilm)=>{
    //     console.log("favoraites :", props.favoriteFilm);
    //        props.navigation.push('FilmDetails', { idFilm: idFilm })
    //     }
    
    return (
      
        <View style={styles.favContainer} >
            <View style={{alignItems:'center'}}>
            <Text>Liste des films favoris</Text>
            </View>
            {console.log("favFilm",props.favoriteFilm)}
            <FilmList
               films={props.favoriteFilm}
                navigation={props.navigation}
                favoriteList={true}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    favContainer: {
        flex: 1,
        marginTop: 4,
    }
})
const MapStateToProps = (state) =>
{
  return {
    favoriteFilm : state.favoriteFilm
  }
}
export default connect(MapStateToProps)(Favorites)