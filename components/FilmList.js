import React from 'react'
import { FlatList } from 'react-native'
import FilmItem from './filmItem'
import{connect } from 'react-redux'
function FilmList(props)
{
    console.log(props);
    const _displayDetailForFIlm = (idFilm)=>{
        console.log("id :", idFilm);
           props.navigation.navigate('FilmDetails', { idFilm: idFilm })
        }
    return (
            <FlatList
                    data={props.films}
                    extraData={props.favoriteFilm}
                    renderItem={({ item }) =>
                    <FilmItem
                        isFilmFavorite={(props.favoriteFilm.findIndex(film => film.id === item.id) !== -1) ? true : false}
                        film={item}
                        displayDetailsForFilm={_displayDetailForFIlm}
                      />}
                        keyExtractor={item => item.id.toString()}
                        onEndReachedThreshold={0.5 }
                        onEndReached={() =>{ 
                         console.log('onEndReacheeeedd');  
                         if (!props.favoriteList && props.page < props.totalPages) {
                          props.loadFilms()
                        }
                    }}
                />
    )
}
const MapStateToProps = (state) =>
{
  return {
    favoriteFilm : state.favoriteFilm
  }
}
export default connect(MapStateToProps)(FilmList)