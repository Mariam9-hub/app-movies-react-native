import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native';
import Search from '../components/search'
import FilmDetails from '../components/filmDetails'
import Favorites from '../components/favorits'
import { Image } from 'react-native';
const Stack = createStackNavigator();

const RootStack = ()=> {
     return (
    <Stack.Navigator>
         <Stack.Screen 
          name="Search"
         component={Search}
         options={{ title: 'Rechercher' }}
         />
         <Stack.Screen 
          name="FilmDetails"
           component={FilmDetails}
           options={{ title: 'Détails du film'}}
         />
         <Stack.Screen 
          name="Favoris"
           component={Favorites}
           options={{ title: 'Films favoris'}}
        />
    </Stack.Navigator>
  );
}
const Tab = createBottomTabNavigator();
function MyTabs() {
  return (
    <Tab.Navigator screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === 'Search') {
          iconName = require('../Images/_ic_search.png');
        } else if (route.name === 'Favorites') {
          iconName =require('../Images/ic_favorite_border.png')
        }

        // You can return any component that you like here!
        return <Image style={{height : 30, width : 30}}  source={iconName} />;
      },
    })}
    tabBarOptions={{
      activeBackgroundColor: '#DDDDDD',
      inactiveBackgroundColor: '#FFFFFF',
      showLabel : false
    }}>
      <Tab.Screen name="Search" component={RootStack} />
      <Tab.Screen name="Favorites" component={Favorites} />
    </Tab.Navigator>
  );
}

export default () =>
{
 return ( <NavigationContainer>
    <MyTabs />
  </NavigationContainer>)
}