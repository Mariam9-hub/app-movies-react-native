const API_TOKEN = 'e32f80cb31cf33d684edc424374a2468'
export function getFilmsFromApiWithSearchedText(text, page)
{
    const url ='https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&query=' + text+'&page='+page
    return fetch(url)
        .then(response =>response.json())
        .catch((error)=> console.log(error))
}
export function getImageFromApi(name)
{
    return 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2' + name
}

export function getDetailsFilmFromApi(idFilm)
{
    const url ='https://api.themoviedb.org/3/movie/' + idFilm + '?api_key=' + API_TOKEN
    return fetch(url)
        .then(response =>response.json())
        .catch((error)=> console.log(error))
}